import React from "react";
import styled from "styled-components";
import { useParams } from "react-router-dom";

import { useQuery, gql } from "@apollo/client";

interface resp {
  id: string;
}

export const DetailScreen: React.FC = () => {
  // getting ID from the router
  let res: resp = useParams();
  const { data, error } = useQuery(
    gql`
      query character($id: ID!) {
        character(id: $id) {
          id
          name
          status
          gender
          origin {
            name
            type
            dimension
          }
          image
        }
      }
    `,
    { variables: { id: res.id } }
  );

  if (error) {
    console.log(error);
  }

  if (data) {
    return (
      <>
        <MainContainer data-testid="DetailScreenMainContainer">
          <TextContainer>
            <h3>Name: {data.character.name}</h3>
            <h3>Status: {data.character.status}</h3>
            <h3>gender: {data.character.gender}</h3>
            <h3>
              origin: {data.character.origin.name}, {data.character.origin.type}
              , {data.character.origin.dimension}
            </h3>
          </TextContainer>
          <Image src={data.character.image} alt={data.character.name} />
        </MainContainer>
      </>
    );
  }

  return <h3>Loading...</h3>;
};

const MainContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  width: 80%;
`;

const TextContainer = styled.div`
  padding: 5%;
`;

const Image = styled.img`
  width: auto;
  height: 60%;
  align-self: center;
`;
