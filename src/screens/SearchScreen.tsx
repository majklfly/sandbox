import React from "react";
import pic from "../assets/rick.png";
import { SearchField } from "../components/SearchField";

interface props {}

export const SearchScreen: React.FC<props> = () => {
  return (
    <div data-testid="SearchScreenContainer">
      <img src={pic} alt="homePic" />
      <SearchField />
    </div>
  );
};
