import { render, act } from "@testing-library/react";
import { DetailScreen } from "./DetailScreen";
import { MockedProvider } from "@apollo/client/testing";
import { Router } from "react-router-dom";
import { gql } from "@apollo/client";

const charactersQuery = gql`
  query character($id: ID!) {
    character(id: $id) {
      id
      name
      status
      gender
      origin {
        name
        type
        dimension
      }
      image
    }
  }
`;

const mocks = [
  {
    request: {
      query: charactersQuery,
    },
    result: {
      data: {
        character: {
          id: "1",
          name: "Rick Sanchez",
          status: "Alive",
          gender: "Male",
          origin: {
            dimension: "Dimension C-137",
            name: "Earth (C-137)",
            type: "Planet",
          },
          image: "https://rickandmortyapi.com/api/character/avatar/1.jpeg",
        },
      },
    },
  },
];

describe("<DetailScreen />", () => {
  let wrapper: any;
  let historyMock: any;

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      location: {},
      listen: jest.fn(),
      params: 1,
    };
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Router history={historyMock}>
          <DetailScreen />
        </Router>
      </MockedProvider>
    );
  });

  it("should render the detail screen", async () => {
    await act(async () => {
      await wrapper.findByTestId("DetailScreenMainContainer");
      wrapper.getByText(/Rick Sanchez/i);
      wrapper.getByText(/Alive/i);
      wrapper.getByText(/Male/i);
    });
  });
});
