import { fireEvent, render, act } from "@testing-library/react";
import { SearchScreen } from "./SearchScreen";
import { MockedProvider } from "@apollo/client/testing";
import { getCharactersQuery } from "../graphql/queries/getCharactersQuery";
import { Router } from "react-router-dom";

const mocks = [
  {
    request: {
      query: getCharactersQuery,
    },
    result: {
      data: {
        characters: {
          results: [
            { id: 1, name: "Rick Sanchez" },
            { id: 2, name: "testName2" },
          ],
        },
      },
    },
  },
];

describe("<SearchScreen />", () => {
  let wrapper: any;
  let historyMock: any;

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      location: {},
      listen: jest.fn(),
    };
    wrapper = render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Router history={historyMock}>
          <SearchScreen />
        </Router>
      </MockedProvider>
    );
  });

  it("should render the screen", () => {
    wrapper.getByTestId("SearchScreenContainer");
  });

  it("should show the error message", () => {
    const input = wrapper.getByTestId("searchInput");
    fireEvent.change(input, "test error Value");
    wrapper.getByAltText("homePic");
  });

  it("should push to the new page after filling correct informations", async () => {
    await act(async () => {
      const input = wrapper.getByTestId("searchInput");
      const correctValue = "Rick Sanchez";
      await fireEvent.change(input, { target: { value: correctValue } });
      await new Promise((resolve) => setTimeout(resolve, 1));
      const button = wrapper.getByTestId("searchFieldSubmitButton");
      await fireEvent.click(button);
      await new Promise((resolve) => setTimeout(resolve, 1));
      expect(historyMock.push.mock.instances[0].push).toHaveBeenCalledWith(
        "/1"
      );
    });
  });
});
