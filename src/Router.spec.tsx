import { render, act, fireEvent, screen } from "@testing-library/react";
import { Router } from "react-router-dom";
import { MockedProvider } from "@apollo/client/testing";
import { RouteredApp } from "./Router";
import { getCharactersQuery } from "./graphql/queries/getCharactersQuery";

const mocks = [
  {
    request: {
      query: getCharactersQuery,
    },
    result: {
      data: {
        characters: {
          results: [
            { id: 1, name: "Rick Sanchez" },
            { id: 2, name: "testName2" },
          ],
        },
      },
    },
  },
];

describe("<Router />", () => {
  let wrapper: any;
  let historyMock: any;

  beforeEach(() => {
    historyMock = {
      push: jest.fn(),
      location: {},
      listen: jest.fn(),
    };

    wrapper = render(
      <MockedProvider
        mocks={mocks}
        defaultOptions={{
          watchQuery: { fetchPolicy: "no-cache" },
          query: { fetchPolicy: "no-cache" },
        }}
      >
        <Router history={historyMock}>
          <RouteredApp />
        </Router>
      </MockedProvider>
    );
  });

  it("should land bad link to search page", () => {
    wrapper.getByAltText("homePic");
  });

  it("should an error message", async () => {
    await act(async () => {
      const button = wrapper.getByTestId("searchFieldSubmitButton");
      await fireEvent.click(button);
      await new Promise((resolve) => setTimeout(resolve, 0));
      wrapper.getByText("Gotta have a typo there...");
    });
  });

  it("should display the value after typing", async () => {
    await act(async () => {
      const input = wrapper.getByTestId("searchInput");
      const correctValue = "Rick Sanchez";
      await fireEvent.change(input, { target: { value: correctValue } });
      await new Promise((resolve) => setTimeout(resolve, 0));
      screen.getByDisplayValue(correctValue);
    });
  });
});
