import React, { useState } from "react";
import styled from "styled-components";
import { useQuery } from "@apollo/client";
import { getCharactersQuery } from "../graphql/queries/getCharactersQuery";
import { useHistory } from "react-router-dom";
import { useForm, FieldValues } from "react-hook-form";

export const SearchField: React.FC = () => {
  const [errorMessage, setErrorMessage] = useState<string>(" ");
  const { error, data } = useQuery(getCharactersQuery);
  const { register, handleSubmit } = useForm<FieldValues>();
  const history = useHistory();

  const onSubmit = (inputValue: FieldValues) => {
    let searchedValue = 0;
    if (data) {
      data.characters.results.forEach((item: any) => {
        if (item.name === inputValue.example) {
          searchedValue = item.id;
        }
      });
    }

    searchedValue !== 0
      ? navigateToNewScreen(searchedValue)
      : setErrorMessage("Gotta have a typo there...");
    const timeout = setTimeout(() => {
      setErrorMessage(" ");
      clearTimeout(timeout);
    }, 2000);
  };

  const navigateToNewScreen = (value: number) => {
    history.push("/" + value);
    setErrorMessage(" ");
  };

  if (error) {
    console.log(error);
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Input
        type="text"
        list="pickerOptions"
        {...register("example")}
        data-testid="searchInput"
      />
      <datalist id="pickerOptions">
        {data?.characters.results.map((value: any, key: number) => {
          return (
            <option key={key} value={value.name}>
              {value.name}
            </option>
          );
        })}
      </datalist>
      <button
        onClick={() => handleSubmit(onSubmit)}
        data-testid="searchFieldSubmitButton"
      >
        Bring it on!
      </button>
      <div style={{ height: 100, width: "100%" }}>
        <ErrorMessage>{errorMessage}</ErrorMessage>
      </div>
    </form>
  );
};

const Input = styled.input`
  width: 100%;
  height: 60px;
  text-align: center;
  font-family: "Roboto";
  font-size: 1.6rem;
`;

const ErrorMessage = styled.h4`
  color: #961900;
  text-align: "center";
`;
