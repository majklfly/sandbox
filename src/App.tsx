import "./App.css";
import { RouteredApp } from "./Router";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <RouteredApp />
      </header>
    </div>
  );
}

export default App;
