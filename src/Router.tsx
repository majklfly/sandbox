import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { SearchScreen } from "./screens/SearchScreen";
import { DetailScreen } from "./screens/DetailScreen";

export const RouteredApp = () => {
  return (
    <Router>
      <Switch>
        <Route path="/:id" children={<DetailScreen />} />
        <Route path="/">
          <SearchScreen />
        </Route>
      </Switch>
    </Router>
  );
};
