import { gql } from "@apollo/client";

export const getCharactersQuery = gql`
  query getCharactersQuery {
    characters {
      results {
        name
        id
      }
    }
  }
`;
