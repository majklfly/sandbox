FROM node:14.15.5 as build

WORKDIR /app

COPY . .

RUN npm install --force
RUN npm run build

FROM nginx
COPY --from=build /app/build /usr/share/nginx/html


COPY default.conf.template /etc/nginx/conf.d/default.conf.template


CMD /bin/bash -c "envsubst '\$PORT' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf" && nginx -g 'daemon off;'